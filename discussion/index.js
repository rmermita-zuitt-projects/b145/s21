console.error('ES6 UPDATES');

//1. EXPONENT OPERATOR
console.warn('Exponent Operator:');
// Math.pow() -> return the base to the exponent power, as in base^exponent
//before
console.log(7 ** 3); //343

//update
console.log(Math.pow(7, 3)); //343



//2. TEMPLATE LITERALS
console.warn('Template Literals:');
//Template Literals (``) backticks
//Allows to write strings without using the concatenation operator 
  //placeholders => ${}
let from = 'sir Marts'

let message = 'Hello Batch 145 love ' + from + '!'; 
console.log(message); 

console.log(`Hello Batch 145 love, ${from}!`);

//how to create multi-line strings?
console.log('Hi sirs and mam \n Hello sirs and mam');

console.log(`
	Hello
	hello 
	hello
`); 

//with embedded JS expressions
const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

let name = "John";

//before
let message2 = 'Hello ' + name + "! Welcome to programming!";
console.log('message without template literals: ' + message2);

//using template literals
message2 = `Hello ${name}! Welcome to programming!`;
console.log(`message with template literals: ${message2}`); 


//3. DESTRUCTURE ARRAYS
console.warn('Array Destructuring:');
//- based on the position of the elements

let grades = [89, 87, 78, 96];
let fullName = ["Juan", "Dela", "Cruz"]; 

//Pre-Array Destructuring
console.log(fullName[0]); //Juan
console.log(fullName[1]); //Dela

//Array Destructuring
//let [firstName, middleName, lastName] = fullName; 
let [firstName, , lastName] = fullName; 
console.log(lastName); //Cruz
//console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);
console.log(`Hello ${firstName} ${lastName}! It's nice to meet you again!`);

function addAndSubtract(a, b) {
	return [a+b, a-b];
}

const array = addAndSubtract(4, 2);
console.log(array);

const [sum, difference] = addAndSubtract(4, 2);
console.log(sum);
console.log(difference);

const letters = ['A', 'B', 'C', 'D', 'E'];
//const [a, b, c, d, e] = letters
//const [a, b, ...rest] = letters; //... => spread operator
//console.log(a);
//console.log(rest);

const [...firsts] = letters;
console.log(firsts);

const numbers = [1, 2, 3, 4, 5];
const array2 = [...letters, ...numbers];
console.log(array2);



//4. DESTRUCTURE OBJECTS
console.warn('Object Destructuring:');
//- allows to unpack properties of an object into distinct variables
//- based on the name of the key

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};
console.log(person);

//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello, my name is ${person.givenName} ${person.maidenName} ${person.familyName}!`)

//object destructuring
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello, my name is ${givenName} ${maidenName} ${familyName}!`)

// function printUser(user){
// 	console.log(user);
// }

function printUser({givenName: nickName, familyName}){
	console.log(`I am ${nickName} ${familyName}`);
}

printUser(person);

const person2 = {
	"given name": "James",
	"family name": "Bond"
};

console.log(person2['given name']);
console.log(person2['family name']);
console.log(`I am ${person2['given name']} ${person2['family name']}`);

const person3 = {
	name2: 'Jill',
	age: 27,
	email: 'jill@mail.com',
	address: {
		city: 'Some city',
		street: 'Some street'
	}
};

//const { name, age } = person3;
const { name2, age, email = null } = person3;
console.log(name2);
console.log(age);
console.log(email);



//5. ARROW FUNCTIONS
console.warn('Arrow Functions:');
//Syntax:
//const/let variableName = (parameters) => {statements};

//before
const hello = function(){
	console.log("Hello world!");
}

//using arrow function
const helloAgain = () => {
	console.log("Hello world!")
} 

hello();
helloAgain();

//before
function printFullName(firstName, middleInitial, lastName) {
			console.log(firstName + ' ' + middleInitial + '. ' + lastName);
}

printFullName("John", "D", "Smith");

//using arrow function
const printFullName2 = (firstName, middleInitial, lastName) => {
		console.log(`${firstName} ${middleInitial}. ${lastName}`);
}

printFullName2("John", "D", "Smith");


//Arrow Functions using loops
const students = ["Joy", "Jade", "Judy"];

//before
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

//using arrow function
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

//example 2
//before
let numberMap = numbers.map(function(number){
	return number * number;
})
console.log(numberMap);

//using arrow function
let numberMap2 = numbers.map((number) => {
	return number * number;
})
console.log(numberMap2);


//example 3
//before
let numberFilter = numbers.filter(function(number){
	return number > 2;
})
console.log(numberFilter);

//using arrow function
let numberFilter2 = numbers.filter((number) => {
	return number > 2;
})
console.log(numberFilter2);


//6. =>Implicit Return Statement
console.warn('Arrow Functions: Implicit Return');

//const add = (x, y) => { return x + y };
const add = (x, y) => x + y;
let total = add(1, 2);
console.log(total);



//7. DEFAULT FUNCTION ARGUMENT VALUE
console.warn('Default Function Argument Value:');

const greet = (name = "User") => {
	return `Good morning, ${name}`;
}

console.log(greet("Jake"));
console.log(greet());


//8. CLASS-BASED OBJECT BLUEPRINTS
console.warn('Class-Based Object Blueprints:');
//- allows creation of objects using classes as blueprints
//- the constructor is a special method of a class for creating an object for that class

/*syntax
class className {
	constructor(propertyA, propertyB) {
		this.propertyA = propertyA;
		this.propertyB = propertyB;
	}
}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.year = year;
		this.name = name;
	}
};

//creating a new instance of car object
const myCar = new Car();
console.log(myCar);

//values of properties may be assigned after creation of an object
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptop';
myCar.year = 2021;
console.log(myCar);

//creating new instance of car with initialized values
const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);